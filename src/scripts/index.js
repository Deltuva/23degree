import '../styles/app.sass';

/* All Modules */
import '../scripts/modules/calculator';

/* Tabs */
const tabs = document.querySelector(".tabs");
const tabsBtns = tabs.querySelectorAll(".tabs__btn");
const tabsContents = tabs.querySelectorAll(".tabs__content");

function displayCurrentTab(current) {
  for (let i = 0; i < tabsContents.length; i++) {
    tabsContents[i].style.display = (current === i) ? "block" : "none";
  }
}
displayCurrentTab(0);

tabs.addEventListener("click", event => {
  if (event.target.className === "tabs__btn") {
    for (let i = 0; i < tabsBtns.length; i++) {
      if (event.target === tabsBtns[i]) {
        displayCurrentTab(i);

        //console.log("Tab content:" +i);
        break;
      }
    }
  }
});

console.log('webpack starterkit');
