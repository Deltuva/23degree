'use strict';

const calculator = document.querySelector(".calculator");

const calc = () => {
  const ranges = calculator.querySelector(".range__labels"),
    rangesBtns = ranges.querySelectorAll(".range__labels-item");

  rangesBtns.forEach(range => {
    range.addEventListener("click", (elem) => {
      let activeRangeDot = elem.target;
      let currentActiveRangeDot = activeRangeDot.dataset.area;
      let type = 1;

      showResult(type, currentActiveRangeDot);

      if (!activeRangeDot.classList.contains('selected')) {
        rangesBtns.forEach(c => {
          c.classList.remove("selected");
          c.classList.add("np");
        });
      }

      if (activeRangeDot.classList.contains('np')) {
        activeRangeDot.classList.remove("np");
        activeRangeDot.classList.toggle("selected");
      } else {
        activeRangeDot.classList.remove("selected");
      }
    });
  });

};

const withoutPlan = (plan, num) => {
  if (isNaN(plan)) {
    return 'Not a Number!';
  }
  return num * plan;
};

const calcPlan = () => {
  const plans = calculator.querySelector(".plan__select"),
    plansBtns = plans.querySelectorAll(".plan__select-item");

  plansBtns.forEach(plan => {
    plan.addEventListener("click", (elem) => {
      let activePlan = elem.target;
      //let currentActivePlan = activePlan.dataset.plan;

      if (!activePlan.classList.contains('selected')) {
        plansBtns.forEach(c => {
          c.classList.remove("selected");
          c.classList.add("np");
        });
      }

      if (activePlan.classList.contains('np')) {
        activePlan.classList.remove("np");
        activePlan.classList.toggle("selected");
      } else {
        activePlan.classList.remove("selected");
      }
    });
  });
};

const numWithSpaces = (num) => {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
};

const showResult = (type, num) => {
  const priceArea = calculator.querySelector(".price__one"),
    priceAvg = calculator.querySelector(".price__dbl"),
    priceTotal = calculator.querySelector(".price__sum");

    priceArea.innerHTML     = '' + numWithSpaces(num) +' м2';
    priceAvg.innerHTML      = '' + numWithSpaces(num/ 2) +' тг';
    priceTotal.innerHTML    = '' + numWithSpaces(num * 10) +' тг';
};

// init
calc();
showResult(0, 0);
calcPlan();